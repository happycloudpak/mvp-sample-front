def label = "${UUID.randomUUID().toString()}"

/* -------- functions ---------- */
def notifySlack(STATUS, COLOR) {
	//slackSend (color: COLOR, message: STATUS+" : " +  "${env.JOB_NAME} [${env.BUILD_NUMBER}] (${env.BUILD_URL})")
}

def notifyMail(STATUS, RECIPIENTS) {
//	emailext body: STATUS+" : " +  "${env.JOB_NAME} [${env.BUILD_NUMBER}] (${env.BUILD_URL})",
//	subject: STATUS + " : " + "${env.JOB_NAME} [${env.BUILD_NUMBER}]",
//	to: RECIPIENTS
}

/* ------------------------------ */

def emailRecipients="hiondal@daum.net"

notifySlack("STARTED", "#FFFF00")
notifyMail("STARTED", "${emailRecipients}")
			
podTemplate(
	label: label, 
	containers: [
		//container image는 docker search 명령 이용
		containerTemplate(name: "docker", image: "docker:stable", ttyEnabled: true, command: "cat"),
		containerTemplate(name: "scanner", image: "newtmitch/sonar-scanner", ttyEnabled: true, command: "cat"),
		containerTemplate(name: "helm", image: "dtzar/helm-kubectl:2.12.3", ttyEnabled: true, command: "cat")
	],
	//volume mount
	volumes: [
		hostPathVolume(hostPath: "/var/run/docker.sock", mountPath: "/var/run/docker.sock")
	]
) 
{
	node(label) {
		String stageName = ""
		stageName = "Get Source"	
		stage(stageName) {
			echo "**** START : " + stageName
			git url: "https://gitlab.com/happycloudpak/mvp-sample-front.git", branch: "master", credentialsId: "auth_gitlab"			
		}

		//-- 환경변수 파일 읽어서 변수값 셋팅
		def props = readProperties  file:"./deployment/pipeline-icp.properties"
		def tag = props["version"]
		def dockerRegistry = props["dockerRegistry"]
		def credentialRegistry=props["credentialRegistry"]
		def image = props["image"]
		def baseDeployDir = props["baseDeployDir"]
		def helmRepository = props["helmRepository"]
		def helmChartname = props["helmChartname"]
		def helmRepositoryURI = props["helmRepositoryURI"]
		def credentialRepository = props["credentialRepository"]
        def credentialICP = props["credentialICP"]
		def helmChartfile = "${baseDeployDir}/${helmChartname}-${tag}.tgz"
		def releaseName = props["releaseName"]
		def namespace = props["namespace"]
		def skipStages = props["skipStages"]
		
		try {
			stageName = "Inspect Code"
			stage(stageName) {
				echo "**** START : " + stageName
				if("${skipStages}".contains(stageName)) {
					echo "Skipped: " + stageName
				} else {
					container("scanner") {
						sh "sonar-scanner \
							-Dsonar.projectName=mvp-product  \
						  -Dsonar.projectKey=mvp-product \
						  -Dsonar.sources=. \
						  -Dsonar.host.url=http://169.56.164.250:31412 \
						  -Dsonar.login=a9b9a6b4ff5e0cf6aba6136c45ef4308ea2c56a0"
					}				
				}
			}
			
			stageName = "Build image"	
			stage(stageName) {
				echo "**** START : " + stageName
				container("docker") {
					docker.withRegistry("${dockerRegistry}", "${credentialRegistry}") {
						sh "docker build -f ${baseDeployDir}/Dockerfile -t ${image}:${tag} ."
						sh "docker push ${image}:${tag}"
						sh "docker tag ${image}:${tag} ${image}:latest"
						sh "docker push ${image}:latest"
					}
				}
			}

			stageName = "Inspect Vulnerability"	
			stage(stageName) {
				echo "**** START : " + stageName
				if("${skipStages}".contains(stageName)) {
					echo "Skipped: " + stageName 
				} else {
					container("docker"){
						aquaMicroscanner imageName: "${image}:latest", notCompliesCmd: "", onDisallowed: "ignore", outputFormat: "html"
					}
				}
			}

			//-- 이미 설치한 차트인 경우 upgrade하고, 아니면 신규 설치함
			//-- git에서 CHART파일을 보내는 경우는 CHART파일을 이용하고, 아니면 helm repository를 이용함			 
			stageName = "Deploy-Dev"
			stage( stageName ) {
				echo "**** START : " + stageName
				container("helm") {
					boolean isExist = false

                    //-- install cloudctl CLI 
                    sh "wget -O cloudctl https://169.56.80.70:8443/api/cli/cloudctl-linux-amd64 --no-check-certificate && chmod +x cloudctl"
                    
                    //-- login ICP
                    try {
                        withCredentials(
                            [
                                usernamePassword
                                    (credentialsId: "${credentialICP}", 
                                        usernameVariable: "ICP_ID",
                                        passwordVariable: "ICP_PW" 
                                    )
                            ]
                        ) {
                            sh "./cloudctl login -a https://169.56.80.70:8443 -u ${ICP_ID} -p ${ICP_PW} -n default"
                        }
                    } catch(e) {
                        error("Can't get Login ICP ! Stop process")	//종료                        
                    }  	

					//====== 이미 설치된 chart 인지 검사 =============
					String out = sh script: "helm ls -q --tls", returnStdout: true
					if(out.contains("${releaseName}")) isExist = true
					//===========================				
				
					if(fileExists("${helmChartfile}")) {
						//chart 파일이 있는 경우
						echo "Helm chart exists. !"
						if (isExist) {
							echo "Already installed. I will upgrade it with chart file"
							sh "helm upgrade -f ${baseDeployDir}/config.yaml ${releaseName} ${helmChartfile} --tls"
						} else {
							echo "Install with chart file !"
							sh "helm install -f ${baseDeployDir}/config.yaml ${helmChartfile} --name ${releaseName} --namespace ${namespace} --tls"
						}	
					} else {
						//없는 경우는 helm repository에서 설치
						echo "Helm chart doesn't exist !" 
						
						sh "helm init"	//tiller 설치								
						
						//add repo
						try {
							withCredentials(
								[
									usernamePassword
										(credentialsId: "${credentialRepository}", 
											usernameVariable: "helmRepositoryID",
											passwordVariable: "helmRepositoryPW" 
										)
								]
							) {
								sh "helm repo add ${helmRepository} ${helmRepositoryURI}  \
									--username ${helmRepositoryID}  \
									--password ${helmRepositoryPW}" 								
								
							}

						} catch(e) {
							error("Can't get credential ! Stop process")	//종료
							
						}
													
						sh "helm repo update"		//update chart

						if (isExist) {
							//upgrade
							echo "Already installed. I will upgrade it from helm repository"
							sh "helm upgrade -f ${baseDeployDir}/config.yaml ${releaseName} ${helmRepository}/${helmChartname} --tls"
							
						} else {
							//install
							echo "Install from helm repository !" 
							sh "helm install -f ${baseDeployDir}/config.yaml ${helmRepository}/${helmChartname} --name ${releaseName} --namespace ${namespace} --tls"						
						}						
										
					}						
				}
			}
/*
			stageName = "Deploy-Staging"
			stage( stageName ) {
				echo "**** START : " + stageName
			
			
			}
			
			stage( "Approve deploy" ){
				input "운영계 배포를 승인하시겠습니까?"
			}

			stageName = "Deploy-Production"
			stage( stageName ) {
				echo "**** START : " + stageName
			
			
			}
*/
			echo "**** FINISH ALL STAGES : SUCESS"
						
			notifySlack("${currentBuild.currentResult}", "#00FF00")
			notifyMail("${currentBuild.currentResult}", "${emailRecipients}")
		} catch(e) {
			currentBuild.result = "FAILED"
			notifySlack("${currentBuild.currentResult}", "#FF0000")
			notifyMail("${currentBuild.currentResult}", "${emailRecipients}")
		}
	}
}